#include <stdio.h>
#include <stdlib.h>
#include <time.h>
char **enterlines(int *count);
int main()
{
	char **arr;
	int count = 0, i,j,tmp;
	puts("enter the lines");
	arr = enterlines(&count);
	for (i = 0; i < count-1; i++) 
		for (j = 0; j < count-i-1; j++) 
			if (strlen(arr[j]) > strlen(arr[j + 1]))
			{
				tmp = arr[j]; 
				arr[j] = arr[j + 1]; 
				arr[j + 1] = tmp;
			}
	for (i = 0; i < count; i++)
		printf("%s", arr[i]);
	return 0;
}