#include <stdio.h>
#include <stdlib.h>
#include <time.h>
char **enterlines(int *count);
int main()
{
	char **arr;
	int count = 0, i = 0, randa, randb, tmp;
	puts("enter the lines");
	arr = enterlines(&count);
	srand(time(0));
	for (i = 0; i < count; i++)
	{
		randa = rand() % count;
		randb = rand() % count;
		tmp = arr[randa];
		arr[randa] = arr[randb];
		arr[randb] = tmp;
	}
	for (i = 0; i < count; i++)
		printf("%s", arr[i]);
	return 0;
}