#pragma once
class Matrix
{
public:
	Matrix();
	Matrix(int, int);
	Matrix(const Matrix&);
	~Matrix();
	void printM();
	Matrix operator+(const Matrix&) const;
	Matrix operator-(const Matrix&) const;
	Matrix operator*(const Matrix&) const;
	Matrix operator*(const double) const;
	Matrix operator/(const double) const;
	friend Matrix operator*(double, const Matrix&);
	Matrix& operator=(const Matrix&);
	Matrix& operator+=(const Matrix&);
	Matrix& operator-=(const Matrix&);
	Matrix operator*=(const Matrix&);
	Matrix& operator*=(const double);
	Matrix& operator/=(const double);
	bool operator==(const Matrix&) const;
	bool operator!=(const Matrix&) const; 
	double& operator()(int, int);
	double det();
	Matrix Adj();
	Matrix reverse();
	Matrix transp();
	void randM();
	int Load(char[20]);
	void Save();
private:
	double **data;
	int N, M; //������ ������� NxM
	// N - ������; M - �������
};