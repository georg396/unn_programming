#include "Matrix.h"
#include <iostream>
#include <stdlib.h>
#include <time.h>

using namespace std;

Matrix::Matrix():data(0),N(0),M(0){}

Matrix::Matrix(int N, int M)
{
	data = new double*[this->N = N];
	for (int i = 0; i < N; i++)
		data[i] = new double[this->M = M];

	for (int i = 0; i < N; i++)
		for (int j = 0; j < M; j++)
		data[i][j] = 0;
}

Matrix::Matrix(const Matrix&mtx)
{
	if (mtx.data)
	{
		data = new double*[this->N = mtx.N];
		for (int i = 0; i < N; i++)
			data[i] = new double[this->M = mtx.M];
		for (int i = 0; i < N; i++)
			for (int j = 0; j < M; j++)
				data[i][j] = mtx.data[i][j];
	}
	else
	{
		data = 0; N = 0; M = 0;
	}
}

Matrix::~Matrix()
{
	for (int i = 0; i < N; i++)
		delete[] data[i];
	delete[] data;
}

void Matrix::printM()
{
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < M; j++)
			printf("%6.2f ", data[i][j]);
		cout << endl;
	}
}

Matrix Matrix::operator+(const Matrix&mtx) const
{
	if (this->N != mtx.N || this->M != mtx.M)
	{
		cout << "matrix sizes do not match" << endl;
		exit(1);
	}
	else
	{
		Matrix result(N,M);
		for (int i = 0; i < N; i++)
			for (int j = 0; j < M; j++)
				result.data[i][j] = data[i][j] + mtx.data[i][j];
		return result;
	}
}

Matrix Matrix::operator-(const Matrix&mtx) const
{
	if (this->N != mtx.N || this->M != mtx.M)
	{
		cout << "matrix sizes do not match" << endl;
		exit(1);
	}
	else
	{
		Matrix result(N,M);
		for (int i = 0; i < N; i++)
			for (int j = 0; j < M; j++)
				result.data[i][j] = data[i][j] - mtx.data[i][j];
		return result;
	}
}

Matrix Matrix::operator*(const Matrix&mtx) const
{
	if (this->M != mtx.N)
	{
		cout << "the multiplication condition isn't satisfied" << endl;
		exit(1);
	}
	else
	{
		Matrix result(N, mtx.M);
		for (int i = 0; i < N; i++)
			for (int j = 0; j < mtx.M; j++)
				for (int k = 0; k < mtx.M; k++)
					result.data[i][j] += data[i][k] * mtx.data[k][j];
		return result;
	}
}

Matrix Matrix::operator*(const double value) const
{
	Matrix result(N, M);

	for (int i = 0; i < N; i++)
		for (int j = 0; j < M; j++)
			result.data[i][j] = data[i][j] * value;

	return result;
}

Matrix Matrix::operator/(const double value) const
{
	Matrix result(N, M);
	for (int i = 0; i < N; i++)
		for (int j = 0; j < M; j++)
			result.data[i][j] = data[i][j] / value;

	return result;
}

Matrix& Matrix::operator=(const Matrix&mtx)
{
	if (this != &mtx && mtx.data)
	{
		for (int i = 0; i < N; i++)
			delete[] data[i];
		delete[] data;

		data = new double*[N = mtx.N];
		for (int i = 0; i < N; i++)
			data[i] = new double[M = mtx.M];

		for (int i = 0; i < N; i++)
			for (int j = 0; j < M; j++)
			data[i][j] = mtx.data[i][j];
	}
	else  if (this != &mtx && !mtx.data)
	{
		for (int i = 0; i < N; i++)
			delete[] data[i];
		delete[] data;
		data = 0;
		N = 0; M = 0;
	}
	return *this;
}

Matrix& Matrix::operator+=(const Matrix& mtx)
{
	if (this->M != mtx.M || this->N != mtx.N)
	{
		cout << "matrix sizes do not match" << endl;
		exit(1);
	}

	else
	{
		for (int i = 0; i < N; i++)
			for (int j = 0; j < M; j++)
				data[i][j] += mtx.data[i][j];
	}

	return *this;
}

Matrix& Matrix::operator-=(const Matrix& mtx)
{
	if (this->M != mtx.M || this->N != mtx.N)
	{
		cout << "matrix sizes do not match" << endl;
		exit(1);
	}

	else
	{
		for (int i = 0; i < N; i++)
			for (int j = 0; j < M; j++)
				data[i][j] -= mtx.data[i][j];
	}

	return *this;
}

Matrix Matrix::operator*=(const Matrix& mtx)
{

	if (this->M != mtx.N)
	{
		cout << "the multiplication condition isn't satisfied" << endl;
		exit(1);
	}

	else
	{
		Matrix result(this->N, mtx.M);
		for (int i = 0; i < N; i++)
			for (int j = 0; j < mtx.M; j++)
				for (int k = 0; k < mtx.M; k++)
					result.data[i][j] += data[i][k] * mtx.data[k][j];
		return result;
	}
}

Matrix& Matrix::operator*=(const double value)
{
	for (int i = 0; i < N; i++)
		for (int j = 0; j < M; j++)
			data[i][j] *= value;
	return *this;
}

Matrix& Matrix::operator/=(const double value)
{
	for (int i = 0; i < N; i++)
		for (int j = 0; j < M; j++)
			data[i][j] /= value;
	return *this;
}

bool Matrix::operator==(const Matrix&mtx) const
{
	if (this->N != mtx.N || this->M != mtx.M)
	{
		cout << "matrix sizes do not match" << endl;
		exit(1);
	}
	for (int i = 0; i < N; i++)
		for (int j = 0; j < M; j++)
			if (data[i][j] != mtx.data[i][j])
				return false;
	return true;
}

bool Matrix::operator!=(const Matrix&mtx) const
{
	if (this->N != mtx.N || this->M != mtx.M)
	{
		cout << "matrix sizes do not match" << endl;
		exit(1);
	}
	for (int i = 0; i < N; i++)
		for (int j = 0; j < M; j++)
			if (data[i][j] != mtx.data[i][j])
				return true;
	return false;
}

double& Matrix::operator()(int i, int j)
{
	if (N && M && i >= 1 && i <= N && j >= 1 && j <= M)
		return data[i-1][j-1];
	else
	{
		cout << "access violation" << endl;
		exit(1);
	}
}

double Matrix::det()
{
	if (N != M)
	{
		cout << "matrix not square" << endl;
		exit(1);
	}
	else
	{
		double det = 1;
		Matrix copy = *this;
		for (int i = 0; i < N; i++)
		{
			for (int j = i + 1; j < N; j++)
			{
				if (copy.data[i][i] == 0)
					return 0;
				double b = copy.data[j][i] / copy.data[i][i];
				for (int k = i; k < N; k++)
					copy.data[j][k] = copy.data[j][k] - b * copy.data[i][k];
			}
			det *= copy.data[i][i];
		}
		return det;
	}
}

Matrix Matrix::Adj()
{
	if (N != M)
	{
		puts("matrix not square");
		exit(1);
	}
	else
	{
		Matrix result(N, N), Temp(N-1, N-1);
		for (int i = 0; i < N; i++)
			for (int j = 0; j < N; j++)
			{
				int ki = 0, kj = 0;
				for (int k = 0; k < N; k++)
				{
					if (k != i)
					{
						kj = 0;
						for (int l = 0; l < N; l++)
						{
							if (l != j)
							{

								Temp.data[ki][kj] = data[k][l];
								kj++;

							}
						}
						ki++;
					}
				}
				result.data[j][i] = Temp.det()*pow(-1, i + j);
			}
		return result;
	}
}

Matrix Matrix::reverse()
{
	if (N != M)
	{
		cout << "matrix not square" << endl;
		exit(1);
	}
	else if (det() == 0)
	{
		cout << "determinant = 0, reverse matrix doesn't exist" << endl;
		exit(1);
	}
	else
	{
		Matrix result = Adj() / det();
		return result;
	}
}

Matrix Matrix::transp()
{ 
	Matrix result(M, N);

	for (int i = 0; i < N; i++)
		for (int j = 0; j < M; j++)
			result.data[j][i] = data[i][j];
	return result;
}

void Matrix::randM()
{
	srand(time(0));
	for (int i = 0; i < N; i++)
		for (int j = 0; j < M; j++)
		{
			data[i][j] = rand() % 10 + 1;
		}
}

int Matrix::Load(char namef[20])
{
	FILE *fp = fopen(namef, "rt");
	if (fp == NULL)
	{
		perror("");
		cout << "Check name of file..." << endl;
		return 1;
	}

	int ch, inWord = 0, Mcount = 0;
	while ((ch = fgetc(fp))!='\n')
	{
		if (ch != ' '&&inWord == 0)
		{
			inWord = 1;
			Mcount++;
		}
		else if (ch == ' '&&inWord == 1)
			inWord = 0;
	}

	rewind(fp);
	char str[256], *token;
	int i = 0, j, Ncount = 0;

	while (fgets(str, 256, fp))
		Ncount++;
	rewind(fp);

	data = new double*[this->N = Ncount];
	for (int i = 0; i < N; i++)
		data[i] = new double[this->M = Mcount];

	while (fgets(str, 256, fp))
	{
		token = strtok(str, " ");
		j = 0;
		while (token)
		{
			data[i][j++] = atof(token);
			token = strtok(NULL, " ");
		}
		i++;
	}

	cout << "The matrix was successfully read from the file" << endl;
	return 0;

}

void Matrix::Save()
{
	FILE *fp = fopen("mtx.txt", "wt");

	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < M; j++)
			fprintf(fp, "%6.2f ", data[i][j]);
		fprintf(fp, "\n");
	}
}