#include "Matrix.h"
#include <iostream>
#include <stdio.h>
using namespace std;

Matrix operator*(double value, const Matrix& mtx)
{
	Matrix result(mtx.N, mtx.M);

	for (int i = 0; i < mtx.N; i++)
		for (int j = 0; j < mtx.M; j++)
			result.data[i][j] = value * mtx.data[i][j];

	return result;
}

int main()
{
	cout << "this is demo program for test the matrix class" << endl;
	cout << "input size of random matrix through the space:" << endl;
	int N, M;
	cin >> N >> M;
	Matrix A(N, M);
	Matrix B;
	char namef[20];
	cout << "input name of file with matrix" << endl;
	cin >> namef;

	cout << "Matrix A:" << endl;
	A.randM();
	A.printM();
	cout << endl;

	cout << "Matrix B from file:" << endl;
	B.Load(namef);
	B.printM();
	cout << endl;

	double num, det;
	Matrix C;

	while (1)
	{
	cout << "choose the operation" << endl;
	cout << "1. A+B" << endl << "2. A-B" << endl << "3. A*B" << endl << "4. A * number" << endl;
	cout << "5. A / number" << endl << "6. compare" << endl << "7. print a element of matrix" << endl;
	cout << "8. determinant A" << endl << "9. reverse A" << endl << "10. transpose A" << endl;
	cout << "11. save to file" << endl << "12. exit" << endl;
	int choise;
	cin >> choise;


		switch (choise)
		{
		default: cout << "Input error! Try again!" << endl; break;
		case 1:
			C = A + B;
			C.printM();
			break;
		case 2:
			C = A - B;
			C.printM();
			break;
		case 3:
			C = A * B;
			C.printM();
			break;
		case 4:
			cout << "input the number:"<<endl;
			cin >> num; cout << endl;
			C = A * num;
			C.printM();
			break;
		case 5:
			cout << "input the number:";
			cin >> num; cout << endl;
			C = A / num;
			C.printM();
			break;
		case 6:
			if (A == B)
				cout << "A=B" << endl;
			else
				cout << "A!=B" << endl;
			break;
		case 7:
			cout << "input number of element through the space:"<<endl;
			cin >> N >> M;
			cout << endl << A(N, M)<< endl;
			break;
		case 8:
			det = A.det();
			cout << det << endl;
			break;
		case 9:
			C = A.reverse();
			C.printM();
			break;
		case 10:
			C = A.transp();
			C.printM();
			break;
		case 11:
			A.Save();
			cout << "done" << endl;
			break;
		case 12: exit(1);
		}
	}
	return 0;
}