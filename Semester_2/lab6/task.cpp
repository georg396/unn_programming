#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <locale.h>
struct THEFILE
{
	//hm - how many
	int hmsymb;
	int famous;
	int hmdig;
	int hmpunct;
	int hmword;
	float avglen;
};
void printstruct(struct THEFILE *ptr)
{
	printf("���������� ��������: %d\n", ptr->hmsymb);
	printf("����� ���������� ������: %c\n", ptr->famous);
	printf("���������� �����: %d\n", ptr->hmdig);
	printf("���������� ������ ����������: %d\n", ptr->hmpunct);
	printf("���������� ����: %d\n", ptr->hmword);
	printf("������� ����� �����: %.2f\n", ptr->avglen);
}
int main()
{
	setlocale(LC_ALL, "rus");
	int arr[256] = {0};
	char name[50];
	struct THEFILE file1;
	FILE *fp;
	file1.hmsymb = 0; file1.famous = 0; file1.hmdig = 0;
	file1.hmpunct = 0; file1.hmword = 0; file1.avglen = 0;
	puts("enter name of file");
	fgets(name, 50, stdin);
	name[strlen(name) - 1] = '\0';
	fp = fopen(name, "r");
	if (fp == NULL)
	{
		perror("File error!");
		exit(1);
	}
	int ch, flag, inWord = 0, wordlen = 0, temp = 0, famous = -1;
	while ((ch = fgetc(fp)) != EOF)
	{
		file1.hmsymb++;
		arr[ch]++;
		if (isdigit(ch))
			file1.hmdig++;
		if (ispunct(ch))
			file1.hmpunct++;
		if ((ch != ' ') && (ch != '\t') && (ch != '\n') && (inWord == 0))
		{
			inWord = 1;
			file1.hmword++;
			wordlen = 1;
		}
		else if ((ch != ' ') && (ch != '\t') && (ch != '\n') && (inWord == 1))
		{
			wordlen++;
			flag = ch;
		}
		else if ((ch == ' ') | (ch == '\t') | (ch == '\n') && inWord == 1)
		{
			temp = temp + wordlen;
			inWord = 0;
			flag = 0;
		}
	}
	if (flag != 0)
		temp = temp + wordlen;
	file1.avglen = temp / file1.hmword;
	for (int i = 0; i<256; i++)
	{
		if (arr[i]>famous)
		{
			famous = arr[i];
			file1.famous = i;
		}
	}
	printstruct(&file1);
	return 0;
}