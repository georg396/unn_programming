#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#include <string.h>
struct BOOK //�������� ��������� ��� ����
{
	char title[30];
	char author[50];
	int year;
};
typedef struct BOOK SBOOK;
void printBOOK(SBOOK *ptr) //������� ��� ������ ���������� � ������
{
	printf("Title:%s", ptr->title);
	printf("Author:%s", ptr->author);
	printf("Year:%d\n", ptr->year);
	printf("\n");
}
int main()
{
	int count = 10, i = 0, j, word = 1; char buf[50];
	int oldesti = 0, newesti = 0, oldyear = 5000, newyear = -1;
	SBOOK *arr;
	FILE *fp;
	setlocale(LC_ALL, "rus");
	
	fp = fopen("books.txt", "r");
	if (fp == NULL)
	{
		perror("FILE ERROR!");
		exit(1);
	}
	arr = (SBOOK*)malloc(count*sizeof(SBOOK));
	while (fgets(buf, 50, fp))
	{
		if (i == count - 1)
			arr = (SBOOK*)realloc(arr, sizeof(SBOOK)*(count += 10));
		if (word == 1)
		{
			strcpy(arr[i].title, buf);
			word++;
		}
		else if (word == 2)
		{
			strcpy(arr[i].author, buf);
			word++;
		}
		else if (word == 3)
		{
			arr[i].year = atoi(buf);
			if (arr[i].year > newyear)
			{
				newyear = arr[i].year;
				newesti = i;
			}
			if (arr[i].year < oldyear)
			{
				oldyear = arr[i].year;
				oldesti = i;
			}
			word = 1;
			i++;
		}
	}
	puts("newest book:");
	printBOOK(&arr[newesti]);
	puts("oldest book:");
	printBOOK(&arr[oldesti]);
	return 0;
}
