#pragma once
class Circle
{
private:
	double Radius;
	double Ference;
	double Area;
public:
	void setRadius(double value);
	void setFerence(double value);
	void setArea(double value);
	void calcFerence_Area();
	void calcRadius_Area();
	void calcRadius_Ference();
	double getRadius();
	double getFerence();
	double getArea();
};

