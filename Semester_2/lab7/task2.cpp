#include <stdio.h>
#include "Circle.h"
#define POOLRAD 3
#define LANERAD 1
#define COVERPRICE 1000
#define FENCEPRICE 2000
int main()
{ 
	Circle PoolandLane;
	PoolandLane.setRadius(POOLRAD + LANERAD);
	PoolandLane.calcFerence_Area();
	Circle Pool;
	Pool.setRadius(POOLRAD);
	Pool.calcFerence_Area();
	printf("Lane Cover Price: %.2f p\n", (PoolandLane.getArea() - Pool.getArea()) * COVERPRICE);
	printf("Fence Price: %.2f p\n", PoolandLane.getFerence() * FENCEPRICE);
	return 0;
}