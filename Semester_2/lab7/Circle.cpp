#include "Circle.h"
#include <math.h>
#define PI 3.14159265359
void Circle::setRadius(double value)
{
	Radius = value;
}
void Circle::setFerence(double value)
{
	Ference = value;
}
void Circle::setArea(double value)
{
	Area = value;
}
void Circle::calcFerence_Area()
{
	Ference = 2 * PI * Radius;
	Area = PI * Radius * Radius;
}
void Circle::calcRadius_Area()
{
	Radius = Ference / (2 * PI);
	Area = PI * Radius * Radius;
}
void Circle::calcRadius_Ference()
{
	Radius = sqrt(Area / PI);
	Ference = 2 * PI * Radius;
}
double Circle::getRadius()
{
	return Radius;
}
double Circle::getFerence()
{
	return Ference;
}
double Circle::getArea()
{
	return Area;
}