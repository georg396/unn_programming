#include <stdio.h>
#include "Circle.h"
#define EarthRad 6378.1

int main()
{
	Circle C1;
	C1.setRadius(EarthRad);
	C1.calcFerence_Area();
	C1.setFerence(C1.getFerence() + 0.001);
	C1.calcRadius_Area();
	printf("%f\n", C1.getRadius() - EarthRad);
	return 0;
}