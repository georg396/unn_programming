#include <stdio.h>
#include <string.h>

int main()
{
	int	j, i = 0;
	int count = 0;
	int str, temp;
	char s[100] = "\0";
	printf("enter the string\n");
	scanf("%d", &str);
	while (str != 0)
	{
		s[i] = str % 10 + '0';
		count++;
		i++;
		if (count % 3 == 0)
		{
			s[i] = ' ';
			i++;
		}
		str = str / 10;
	}
	for (i = 0, j = strlen(s) - 1; i < j; i++, j--)
	{
		temp = s[j];
		s[j] = s[i];
		s[i] = temp;
	}
	printf("%s\n", s);
}