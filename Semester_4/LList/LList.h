#pragma once

struct countries
{
	char UNcode[4];
	char twoISOabbr[3];
	char threeISOabbr[4];
	char Name[256];
	char Capital[256];
} typedef DataType;

class LList
{
private:
	struct ITEM
	{
		DataType *data;
		ITEM* next;
	};
	LList::ITEM* create(const DataType&);
	ITEM* head;
	ITEM* tail;
public:
	LList() : head(0), tail(0){}
	LList(const DataType&);
	void addTail(const DataType&);
	void print() const;
	void search(const char* Capital);
};