#include "LList.h"
#include <iostream>

using namespace std;

LList::ITEM* LList::create(const DataType& data)
{
	ITEM* item = new ITEM;
	item->data = new DataType;
	*item->data = data;
	item->next = 0;
	return item;
}

LList::LList(const DataType& data)
{
	head = create(data);
	tail = head;
}

void LList::addTail(const DataType& data)
{
	if (head&&tail)
	{
		tail->next = create(data);
		tail = tail->next;
	}
	else
	{
		head = create(data);
		tail = head;
	}
}

void LList::print() const
{
	ITEM* temp = head;
	while (temp)
	{
		cout << temp->data->UNcode << "," << temp->data->twoISOabbr << "," << temp->data->threeISOabbr << ",";
		cout << temp->data->Name << "," << temp->data->Capital << endl;
		temp = temp->next;
	}
}

void LList::search(const char* Capital)
{
	ITEM* temp = head;
	bool found = 0;
	while (temp)
	{
		if (!strcmp(temp->data->Capital, Capital))
		{
			cout << temp->data->UNcode << "," << temp->data->twoISOabbr << "," << temp->data->threeISOabbr << ",";
			cout << temp->data->Name << "," << temp->data->Capital << endl;
			found = 1;
			break;
		}
		temp = temp->next;
	}
	if (!found) cout << "Record is not found" << endl;
}
