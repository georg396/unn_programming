#include "LList.h"
#include <iostream>
#include <fstream>

using namespace std;

DataType parsing(const char buf[256])
{
	DataType data; int i = 0, temp;
	while (buf[i] != ',') //������ ������� �� ������� ���������� � UNcode
	{
		data.UNcode[i] = buf[i];
		i++;
	}
	data.UNcode[i] = '\0'; i++; temp = i; //����� ������ ������ � ������ char ��������� \0
	while (buf[i] != ',') //��������� ������� �� ������� ���������� � twoISOabbr
	{
		data.twoISOabbr[i - temp] = buf[i];
		i++;
	}
	data.twoISOabbr[i - temp] = '\0'; i++; temp = i;
	while (buf[i] != ',') //��������� ������� �� ������� ���������� � threeISOabbr
	{
		data.threeISOabbr[i - temp] = buf[i];
		i++;
	}
	data.threeISOabbr[i - temp] = '\0'; i++; temp = i;
	while (buf[i] != ',') //��������� ������� �� ������� ��������� � Name
	{
		data.Name[i - temp] = buf[i];
		i++;
	}
	data.Name[i - temp] = '\0'; i++; temp = i;
	while (buf[i] != '\0') //��� ����������� ������� �� ����� ������ ���������� � Capital
	{
		data.Capital[i - temp] = buf[i];
		i++;
	}
	data.Capital[i - temp] = '\0';
	return data;
}

int main()
{
	char buf[256], Capital[100];
	LList list;
	ifstream file("countries.csv");
	if (!file.is_open())
		cout << "File won't open!\n";
	file.getline(buf, 256);
	while (file.getline(buf, 256))
		list.addTail(parsing(buf));
	list.print();
	cout << endl << "Enter Capital" << endl;
	cin >> Capital;
	list.search(Capital);
	file.close();
	return 0;
}