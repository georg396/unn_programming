#include "Tree.h"

Tree::NODE* Tree::addNode(const DataType& data, Tree::NODE* root)
{
	if (root == NULL)
	{
		root = new Tree::NODE;
		root->data = data;
		root->left = NULL;
		root->right = NULL;
		root->count = 1;
	}
	else if (root->data > data)
		root->left = addNode(data, root->left);
	else if (root->data < data)
		root->right = addNode(data, root->right);
	else
		root->count++;
	return root;
}

Tree::NODE*  Tree::search(const DataType& data, Tree::NODE* root)
{
	if (root != NULL)
	{
		if (root->data == data)
			return root;
		else if (data > root->data)
			return search(data, root->right);
		else if (data < root->data)
			return search(data, root->left);
	}
	return 0;
}

void Tree::print(NODE* root)
{
	if (root->left)
		print(root->left);
	cout << root->data << endl;
	if (root->right)
		print(root->right);
}
