#pragma once
#include <string>
#include <iostream>

using namespace std;

typedef std::string DataType;

class Tree
{
public:
	struct NODE
	{
		int count;
		DataType data;
		NODE* left;
		NODE* right;
	};
	NODE* addNode(const DataType& data, NODE* root);
	NODE* search(const DataType& data, NODE* root);
	void print(NODE* root);
};
