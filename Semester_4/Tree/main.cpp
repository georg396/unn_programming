#include "Tree.h"
#include <fstream>

using namespace std;

int main()
{
	Tree::NODE* root = NULL; 
	Tree::NODE* temp;
	ifstream file;
	file.open("war+peace.txt");
	if (!file.is_open())
		cout << "File can not be open!" << endl;
	else
	{
		string buf;
		Tree tree;
		file >> buf;
		root = tree.addNode(buf, NULL);
		while (file >> buf)
			tree.addNode(buf, root);
		cout << "go" << endl;
		file.close();
		temp = tree.search("Rostov", root);
		if (temp)
			cout << endl << "'Rostov' count = " << temp->count << endl;
		else cout << "Found nothing" << endl;
	}
	return 0;
}
