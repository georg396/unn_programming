#include <iostream>
#include <fstream>
#include <list>
#include <vector>
#include <ctime>

using namespace std;

int main()
{
	list<char> l;
	vector<char> v;
	char buf;
	ifstream file;
	file.open("TomSawyer.txt");
	if (!file.is_open())
		cout << "File can not be open!" << endl;
	else
	{
		while (file >> buf)
		{
			l.push_back(buf);
			v.push_back(buf);
		}
		file.close();
		cout << "Size of file: " << l.size() << endl;
		int time_list1, time_list2; 
		int time_vector1, time_vector2;
		for (int char_number = 500; char_number <= 1500; char_number = char_number + 500)
		{
			//work with list
			time_list1 = clock();
			for (int i = 0; i < char_number; i++)
				l.push_front(char('a'));
			time_list2 = clock();
			
			//work with vector
			time_vector1 = clock();
			for (int i = 0; i < char_number; i++)
				v.insert(v.begin(), (char('a')));
			time_vector2 = clock();

			cout << endl << "Number of insert chars: " << char_number << endl;
			cout << "List time: " << ((float)(time_list2 - time_list1)) / CLOCKS_PER_SEC << " s" << endl;
			cout << "Vector time: " << ((float)(time_vector2 - time_vector1)) / CLOCKS_PER_SEC << " s" << endl;
		}
		
	}

	return 0;
}